class StaticPagesController < ApplicationController
  def home
  end

  def quests
  end
  
  def help
  end

  def about
  end

  def contact
  end

end
