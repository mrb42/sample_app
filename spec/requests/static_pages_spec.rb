require 'spec_helper'

describe "Static pages" do

  subject { page }

  describe "Home page" do
  before { visit root_path }

    it { should have_selector('h1', text: 'HealthQuest') }
    it { should have_selector('title', text: full_title('')) }
    it { should_not have_selector 'title', text: '| Home' }
  end

  describe "Quests Page" do
    before { visit quests_path }

    it { should have_selector('h1', text: 'Quests') }
    it { should have_selector('title', text: full_title('Quests')) }
  end

  describe "Help page" do
    before { visit help_path }

    it { should have_selector('h1', text: 'Help') }
    it { should have_selector('title', text: full_title('Help')) }
  end

  describe "About page" do
    before { visit about_path }

    it { should have_selector('h1', text: 'About') }
    it { should have_selector('title', text: full_title('About')) }
  end

  describe "Contact page" do
  before { visit contact_path }

    it { should have_selector('h1', text: 'Contact') }
    it { should have_selector('title', text: full_title('Contact')) }
  end
  
end